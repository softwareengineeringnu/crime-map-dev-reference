Crime-Map

Since the early 2000s, NYPD has been collecting crime data from around New York City. This project attempts to utilize this data in order to rate google maps routes. The "best" route is returned to the user in an attempt to show a safer route home.

LOCAL TEST INSTRUCTIONs
PYTHON MUST BE INSTALLED

1. Open cmd and navigate to the project directory

2. run "python -m http.server 8000" from the command line

3. Open a web browser and navigate to localhost:8000





~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Webpage layout by HTML5 UP. Readme and credits below:

Read Only by HTML5 UP
html5up.net | @n33co
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)


Just a super simple single-page responsive template built for personal sites and portfolios
(although it'd definitely work for other stuff too). Includes a contact form, pre-styled
elements, and Sass sources.

Demo images* courtesy of Unsplash, a radtastic collection of CC0 (public domain) images
you can use for pretty much whatever.

(* = Not included)

Feedback, bug reports, and comments are not only welcome, but strongly encouraged :)

AJ
n33.co @n33co dribbble.com/n33


Credits:

	Demo Images:
		Unsplash (unsplash.com)

	Icons:
		Font Awesome (fortawesome.github.com/Font-Awesome)

	Other:
		jQuery (jquery.com)
		html5shiv.js (@afarkas @jdalton @jon_neal @rem)
		CSS3 Pie (css3pie.com)
		skel (getskel.com)